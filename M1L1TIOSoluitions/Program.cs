﻿using System;
using System.Collections.Generic;

namespace M1L1TIOSoluitions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Variables
            // Try it out - Try it out! Create a two dimensional array of the table below;

            int[,] table = new int[3,5];

            table[0, 0] = 59;
            table[0, 1] = 68;
            table[0, 4] = 4;
            table[1, 1] = 7;
            table[2, 0] = 96;
            table[2, 4] = 9;

            // OR

            int[,] tableAlt = new int[, ] { { 59,68,0,0,4}, { 0, 7, 0, 0,0 } , { 96, 0, 0, 0 ,9} };

            Console.WriteLine("");

            // Try it out! Store your first name, last name and address in an array and display it to the console.;

            string[] info = new string[3] { "", "", "" };

            info[0] = "Dean";
            info[1] = "von Schoultz";
            info[2] = "53 Made up Avenue";

            // OR

            string[] infoAlt = new string[3] { "Dean", "von Schoultz", "53 Made up Avenue "};

            Console.WriteLine($"{info[0]} {info[1]} {info[2]}");

            // Try it out! Create a jagged array of the table below    

            int[][] jaggedAgeExample = new int[][]
            {
                new int[4],
                new int[2],
                new int[1],
                new int[4]
            };

            jaggedAgeExample[0][1] = 6;
            jaggedAgeExample[0][3] = 9;
            jaggedAgeExample[2][0] = 66;
            jaggedAgeExample[3][2] = 73;

            #endregion

            #region Generic Collections

            //  Try it out! Create a dictionary to store and then display your families hobbies. Use their names as keys.

            Dictionary<string, string> familyHobbies = new Dictionary<string, string>();
            familyHobbies["Tam"] = "Running";
            familyHobbies["Dean"] = "Boxing";
            familyHobbies["Kaami"] = "Rock Climbing";

            // Try it out! Create a List to store at least 6 film names.

            List<string> movies = new List<string>();
            movies.Add("Pulp Fiction");
            movies.Add("Jaws");
            movies.Add("Death Proof");
            movies.Add("Dune");
            movies.Add("Howl's Moving Castle");
            movies.Add("Ninja Scroll");

            // Try it out! Use one of the above collections to display the grocery items below (choose whichever you think is appropriate or interesting)

            // Task: Create a new console application named CollectionSelector. Create an empty Collection of each type covered above

            List<string> listExample = new List<string>();
            Queue<string> queueExample = new Queue<string>();
            Stack<string> stackExample = new Stack<string>();
            Dictionary<string,int> dictionaryExample = new Dictionary<string, int>();
            SortedList<string, int> sortedlistExample = new SortedList<string, int>();

            // Try it out! Use one of the above collections to display the grocery items below (choose whichever you think is appropriate or interesting)

            Stack<string> shoppingListStack = new Stack<string>();
            shoppingListStack.Push("Popcorn");
            shoppingListStack.Push("Oats");
            shoppingListStack.Push("Carrots");
            shoppingListStack.Push("Tofu");

            Console.WriteLine($"- {shoppingListStack.Pop()}");
            Console.WriteLine($"- {shoppingListStack.Pop()}");
            Console.WriteLine($"- {shoppingListStack.Pop()}");
            Console.WriteLine($"- {shoppingListStack.Pop()}");

            //OR

            Queue <string> shoppingListQueue = new Queue<string>();
            shoppingListQueue.Enqueue("Popcorn");
            shoppingListQueue.Enqueue("Oats");
            shoppingListQueue.Enqueue("Carrots");
            shoppingListQueue.Enqueue("Tofu");

            Console.WriteLine($"- {shoppingListQueue.Dequeue()}");
            Console.WriteLine($"- {shoppingListQueue.Dequeue()}");
            Console.WriteLine($"- {shoppingListQueue.Dequeue()}");
            Console.WriteLine($"- {shoppingListQueue.Dequeue()}");

            //OR 

            SortedList<int,string> shoppingListSorted = new SortedList<int,string>();
            shoppingListSorted.Add(2,"Popcorn");
            shoppingListSorted.Add(1,"Oats");
            shoppingListSorted.Add(4,"Carrots");
            shoppingListSorted.Add(3,"Tofu");

            Console.WriteLine($"- {shoppingListSorted[1]}");
            Console.WriteLine($"- {shoppingListSorted[2]}");
            Console.WriteLine($"- {shoppingListSorted[3]}");
            Console.WriteLine($"- {shoppingListSorted[4]}");

            #endregion

            #region Control Structures

            // Try it out! Try and guess the output of the code below. It is in fact incorrect. Try and fix it!

            int num1 = 70;

            if (num1 == 73)
            {
                Console.WriteLine("Number is 73");
            }
            else if (num1 > 73)
            {
                Console.WriteLine("Number is greater than 73");
            }
            else
            {
                Console.WriteLine("Number is less than 73");
            }

            // Try it out! Write a switch statement which checks the name of a month and displays the number of the month.

            string month = "March";

            switch (month)
            {
                case "January":
                    Console.WriteLine("1");
                    break;
                case "February":
                    Console.WriteLine("2");
                    break;
                case "March":
                    Console.WriteLine("3");
                    break;
                case "April":
                    Console.WriteLine("4");
                    break;
                case "May":
                    Console.WriteLine("5");
                    break;
                case "June":
                    Console.WriteLine("6");
                    break;
                case "July":
                    Console.WriteLine("7");
                    break;
                case "August":
                    Console.WriteLine("8");
                    break;
                case "September":
                    Console.WriteLine("9");
                    break;
                case "October":
                    Console.WriteLine("10");
                    break;
                case "November":
                    Console.WriteLine("11");
                    break;
                case "December":
                    Console.WriteLine("12");
                    break;
                default:
                    Console.WriteLine("Incorrect input"); 
                    break;
            }

            //Try it out!Write your name to the console ten times using a for loop

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Dean");
            }

            //Try it out! Display all the contents of one of your previous collections
            foreach (string movie in movies)
            {
                Console.WriteLine(movie);
            }

            foreach (KeyValuePair<int, string> kvp in shoppingListSorted)
            {
                Console.WriteLine($"{kvp.Key}: {kvp.Value}");
                
            }


            // Try it out! Write a while loop which keeps doubling a number starting at 9 until the number is greater than 8052. Printing out the current doubled value during each iteration

            int number = 9;

            while (number < 8052)
            {
                Console.WriteLine(number);

                number = number * 2;
            }

            // Try it out! Write a do-while loop which prompts users to enter numbers, they can continue entering numbers until they enter the word "Total". Then the program should display the sum of all the entered numbers

            string input = "";
            int total = 0;

            do
            {
                Console.WriteLine("Enter a number: (or enter the word Total to exit)");
                input = Console.ReadLine();
                if (input != "Total")
                {
                    total = total + int.Parse(input);
                }

            } while (input != "Total");

            Console.WriteLine($"Total is: {total}");

            // Try it out! Add exception handling to the code below so that out of bounds exceptions write "that is out of bounds" and "an error has occurred" if any other error occurs

            try
            {
                int[] numbers = new int[5];

                numbers[6] = 2;
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("that is out of bounds");
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("an error has occurred"); 
            }

            #endregion

            #region Methods

            // Try it out! Write a method which takes in your name as an argument. It must then print your name out 5 times.
            NamePrinter("Dean");

            // Try it out! Update the Subtract method you wrote previously to return the result instead of printing it out. Then call them to get the same output as before.
            Console.WriteLine(Subtract(10, 5));

            // Task: Copy your Subtract method to a new console application in Visual Studio and comment it so that the intellisense displays the details of the method. SEE SUBTRACT METHOD

            // Try it out!Write two methods with the same name of Subtract but give them each different number of integer parameters(up to you how many).Then call each of them to display the result of subtracting arguments from each other.
            Console.WriteLine(Subtract(100,67,3));
            Console.WriteLine(Subtract(100, 67));
            Console.WriteLine(Subtract(300, Subtract(100, 67, 3)));
            Console.WriteLine(Subtract(Subtract(5000,79), Subtract(100, 67, 3)));

            // Try it out! Write a method which takes in any number of integers as arguments, adds them together and displays the result.
            AddAny(4, 6, 3, 73, 74, 8, 8, 4, 7, 433, 838, 38);
            #endregion

            #region  Variable Scope

            // Try it out! Try to print out the number1 variable in Add from Main.
            //Console.WriteLine(number1);

            #endregion
        }


        public static void NamePrinter(string name)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(name);
            }
        }


        /// <summary>
        /// Subtracts a number from another number
        /// </summary>
        /// <param name="left">Number which requires subtraction</param>
        /// <param name="right">Number to subtract</param>
        /// <returns>The result of right subtracted from left</returns>
        public static int Subtract(int left, int right)
        {

            return left - right;

            // OR

            //int result = left - right;
            //return result;
   
        }

        public static int Subtract(int left, int right, int secondRight)
        {

            return left - (right + secondRight);

            // OR

            //int subtractTotal = right + secondRight;
            //int result = left - subtractTotal;
            //return result;

        }

        public static void AddAny(params int [] numbers)
        {
            int total = 0;

            foreach (int number in numbers)
            {
                total += number;
                //OR
                // total = total + number;
            }

            Console.WriteLine(total);
        }

        public static int Add(int number1, int number2)
        {
            int sum = number1 + number2;

            for (int i = 0; i < sum; i++)
            {
                Console.WriteLine($"Iteration: {i}");
            }

            if (sum > 1000)
            {
                string message = "Sum is greater than 1000";
                Console.WriteLine(message);
            }
            return sum;
        }
    }
}
